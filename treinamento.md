## Treinamento
### Nº Epochs: 16
```
Found 1573 images belonging to 2 classes.
Found 174 images belonging to 2 classes.
{0: 1.1300287356321839, 1: 0.8968072976054732}
Epoch 1/16
49/49 [==============================] - 62s 1s/step - loss: 0.4421 - accuracy: 0.8112 - val_loss: 0.2917 - val_accuracy: 0.8813
Epoch 2/16
49/49 [==============================] - 57s 1s/step - loss: 0.2485 - accuracy: 0.8936 - val_loss: 0.2456 - val_accuracy: 0.9187
Epoch 3/16
49/49 [==============================] - 56s 1s/step - loss: 0.2120 - accuracy: 0.9215 - val_loss: 0.3405 - val_accuracy: 0.8813
Epoch 4/16
49/49 [==============================] - 57s 1s/step - loss: 0.1864 - accuracy: 0.9202 - val_loss: 0.2647 - val_accuracy: 0.9000
Epoch 5/16
49/49 [==============================] - 57s 1s/step - loss: 0.1665 - accuracy: 0.9416 - val_loss: 0.2572 - val_accuracy: 0.9000
Epoch 6/16
49/49 [==============================] - 56s 1s/step - loss: 0.1435 - accuracy: 0.9481 - val_loss: 0.2630 - val_accuracy: 0.8813
Epoch 7/16
49/49 [==============================] - 56s 1s/step - loss: 0.1257 - accuracy: 0.9617 - val_loss: 0.1904 - val_accuracy: 0.9000
Epoch 8/16
49/49 [==============================] - 56s 1s/step - loss: 0.0760 - accuracy: 0.9740 - val_loss: 0.3095 - val_accuracy: 0.9250
Epoch 9/16
49/49 [==============================] - 55s 1s/step - loss: 0.1037 - accuracy: 0.9585 - val_loss: 0.2009 - val_accuracy: 0.9187
Epoch 10/16
49/49 [==============================] - 56s 1s/step - loss: 0.0615 - accuracy: 0.9799 - val_loss: 0.2789 - val_accuracy: 0.9062
Epoch 11/16
49/49 [==============================] - 56s 1s/step - loss: 0.0300 - accuracy: 0.9890 - val_loss: 0.2658 - val_accuracy: 0.9375
Epoch 12/16
49/49 [==============================] - 56s 1s/step - loss: 0.0248 - accuracy: 0.9935 - val_loss: 0.3026 - val_accuracy: 0.9125
Epoch 13/16
49/49 [==============================] - 55s 1s/step - loss: 0.0156 - accuracy: 0.9942 - val_loss: 0.3221 - val_accuracy: 0.9062
Epoch 14/16
49/49 [==============================] - 56s 1s/step - loss: 0.0204 - accuracy: 0.9942 - val_loss: 0.2579 - val_accuracy: 0.9312
Epoch 15/16
49/49 [==============================] - 55s 1s/step - loss: 0.0179 - accuracy: 0.9942 - val_loss: 0.3557 - val_accuracy: 0.9375
Epoch 16/16
49/49 [==============================] - 56s 1s/step - loss: 0.0212 - accuracy: 0.9935 - val_loss: 0.2785 - val_accuracy: 0.9375
```

### Loss
![img.png](documentacao/loss2.png)
### Acertos
![img.png](documentacao/accuracy2.png)

## Exemplo com overfitting
## Nº Epochs: 20

````
Epoch 1/20
49/49 [==============================] - 92s 2s/step - loss: 0.3943 - accuracy: 0.8559 - val_loss: 0.2738 - val_accuracy: 0.8813
Epoch 2/20
49/49 [==============================] - 71s 1s/step - loss: 0.2407 - accuracy: 0.9053 - val_loss: 0.3208 - val_accuracy: 0.8875
Epoch 3/20
49/49 [==============================] - 72s 1s/step - loss: 0.2118 - accuracy: 0.9234 - val_loss: 0.1996 - val_accuracy: 0.9187
Epoch 4/20
49/49 [==============================] - 70s 1s/step - loss: 0.1506 - accuracy: 0.9442 - val_loss: 0.1907 - val_accuracy: 0.9125
Epoch 5/20
49/49 [==============================] - 71s 1s/step - loss: 0.1851 - accuracy: 0.9293 - val_loss: 0.2621 - val_accuracy: 0.9000
Epoch 6/20
49/49 [==============================] - 74s 2s/step - loss: 0.1141 - accuracy: 0.9604 - val_loss: 0.2146 - val_accuracy: 0.9250
Epoch 7/20
49/49 [==============================] - 72s 1s/step - loss: 0.1053 - accuracy: 0.9578 - val_loss: 0.3423 - val_accuracy: 0.8562
Epoch 8/20
49/49 [==============================] - 72s 1s/step - loss: 0.1351 - accuracy: 0.9578 - val_loss: 0.4319 - val_accuracy: 0.8438
Epoch 9/20
49/49 [==============================] - 73s 1s/step - loss: 0.1230 - accuracy: 0.9591 - val_loss: 0.2343 - val_accuracy: 0.9000
Epoch 10/20
49/49 [==============================] - 71s 1s/step - loss: 0.0574 - accuracy: 0.9799 - val_loss: 0.3012 - val_accuracy: 0.9125
Epoch 11/20
49/49 [==============================] - 72s 1s/step - loss: 0.0259 - accuracy: 0.9922 - val_loss: 0.2275 - val_accuracy: 0.9438
Epoch 12/20
49/49 [==============================] - 73s 1s/step - loss: 0.0283 - accuracy: 0.9896 - val_loss: 0.2460 - val_accuracy: 0.9250
Epoch 13/20
49/49 [==============================] - 71s 1s/step - loss: 0.0165 - accuracy: 0.9968 - val_loss: 0.2474 - val_accuracy: 0.9250
Epoch 14/20
49/49 [==============================] - 72s 1s/step - loss: 0.0236 - accuracy: 0.9922 - val_loss: 0.3180 - val_accuracy: 0.9062
Epoch 15/20
49/49 [==============================] - 73s 1s/step - loss: 0.0036 - accuracy: 1.0000 - val_loss: 0.3412 - val_accuracy: 0.9500
Epoch 16/20
49/49 [==============================] - 73s 1s/step - loss: 0.0016 - accuracy: 1.0000 - val_loss: 0.3129 - val_accuracy: 0.9438
Epoch 17/20
49/49 [==============================] - 73s 1s/step - loss: 9.1895e-04 - accuracy: 1.0000 - val_loss: 0.3644 - val_accuracy: 0.9375
Epoch 18/20
49/49 [==============================] - 72s 1s/step - loss: 5.0563e-04 - accuracy: 1.0000 - val_loss: 0.4677 - val_accuracy: 0.9375
Epoch 19/20
49/49 [==============================] - 71s 1s/step - loss: 3.0424e-04 - accuracy: 1.0000 - val_loss: 0.4987 - val_accuracy: 0.9312
Epoch 20/20
49/49 [==============================] - 73s 1s/step - loss: 2.4212e-04 - accuracy: 1.0000 - val_loss: 0.5184 - val_accuracy: 0.9312
````

### Loss
![img.png](documentacao/loss1.png)
### Acertos
![img.png](documentacao/accuracy1.png)
