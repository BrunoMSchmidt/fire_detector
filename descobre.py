import tensorflow as tf
from tensorflow.keras.preprocessing import image
import numpy as np
import cv2

# Dados de treinamento
model_name = 'modelo.h5'

# Carregar o modelo treinado
model = tf.keras.models.load_model(model_name)


def predictAndShow(image_path):
    # Fazer a previsão da imagem
    tem_fogo = predict(image_path)

    # Mensagem a ser exibida
    mensagem = "Ha fogo na imagem." if tem_fogo else "Nao ha fogo na imagem."

    # Mostrar a imagem
    img = cv2.imread(image_path)

    # Redimensionar a imagem para ter no máximo 800 pixels de largura ou altura
    max_size = 800
    if img.shape[0] > max_size or img.shape[1] > max_size:
        scale = max_size / max(img.shape[0], img.shape[1])
        img = cv2.resize(img, (int(img.shape[1] * scale), int(img.shape[0] * scale)))

    # Escrever o texto com bordas pretas para ficar mais visível
    cv2.putText(img=img, text=mensagem, org=(30, 50),
                fontFace=cv2.FONT_HERSHEY_COMPLEX, fontScale=1.5, color=[0, 0, 255], lineType=cv2.LINE_AA,
                thickness=4)
    cv2.putText(img=img, text=mensagem, org=(30, 50),
                fontFace=cv2.FONT_HERSHEY_COMPLEX, fontScale=1.5, color=[255, 255, 255], lineType=cv2.LINE_AA,
                thickness=2)

    # Mostrar a imagem
    cv2.imshow('Imagem: ' + image_path, img)
    cv2.waitKey(0)
    cv2.destroyAllWindows()


def predict(image_path):
    # Carregar a imagem e redimensionar para o tamanho esperado
    img = image.load_img(image_path, target_size=(224, 224))

    # Converter a imagem para um array numpy
    img_array = image.img_to_array(img)

    # Expandir as dimensões para se adequar ao modelo (1 amostra com 3 canais RGB)
    img_array = np.expand_dims(img_array, axis=0)

    # Pré-processamento da imagem (normalização)
    img_array = img_array / 255.0

    # Fazer a previsão da imagem
    prediction = model.predict(img_array)

    # Verificar se a previsão é de fogo ou não
    tem_fogo = prediction[0] <= 0.5
    probabilidade = str((1 - prediction[0]) * 100) + '%'

    image_name = image_path.split('/')[-1]
    print('Imagem: ' + image_name + " - " + (
        "Há fogo na imagem." if tem_fogo else "Não há fogo na imagem.") + ' - Probabilidade de fogo: ' + probabilidade + '\n')

    return tem_fogo


def getImagesFromFolderAndPredict(folder_path, fire):
    wrong = 0
    right = 0

    import os
    for filename in os.listdir(folder_path):
        if filename.endswith(".jpg") or filename.endswith(".png"):
            tem_fogo = predict(folder_path + '/' + filename)
            if tem_fogo == fire:
                right += 1
            else:
                wrong += 1

    return right, wrong


# Teste

# predictAndShow('imagens_para_testes/sem_fogo/max-bender-s4I1xpX_ny8-unsplash.jpg')
# getImagesFromFolderAndPredict('imagens_para_testes/sem_fogo', False)

[acertos, erros] = getImagesFromFolderAndPredict('imagens_para_testes/com_fogo', True)
print('Acertos: ' + str(acertos))
print('Erros: ' + str(erros))

[acertos, erros] = getImagesFromFolderAndPredict('imagens_para_testes/sem_fogo', False)
print('Acertos: ' + str(acertos))
print('Erros: ' + str(erros))
